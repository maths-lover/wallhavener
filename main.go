package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/dustin/go-humanize"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"
)

type WriteCounter struct {
	Total uint64
}

func (wc *WriteCounter) Write(p []byte) (int, error) {
	n := len(p)
	wc.Total += uint64(n)
	wc.PrintProgress()
	return n, nil
}

func (wc WriteCounter) PrintProgress() {
	fmt.Printf("\r%s", strings.Repeat(" ", 50))
	fmt.Printf("\rDownloading... %s complete", humanize.Bytes(wc.Total))
}

type Response struct {
	Data []Wallpaper `json:"data"`
}

type Wallpaper struct {
	ID         string `json:"id"`
	Url        string `json:"url"`
	Purity     string `json:"purity"`
	Category   string `json:"category"`
	Resolution string `json:"resolution"`
	Path       string `json:"path"`
}

func DownloadFile(url string, filepath string) error {
	out, err := os.Create(filepath + ".jpg")
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create our bytes counter and pass it to be used alongside our writer
	counter := &WriteCounter{}
	_, err = io.Copy(out, io.TeeReader(resp.Body, counter))
	if err != nil {
		return err
	}

	fmt.Println()
	err = os.Rename(filepath+".jpg", filepath)
	if err != nil {
		return err
	}
	return nil
}

func main() {
	category := flag.String("category", "100", "Category for Wallpaper search (General/Anime/People)")
	purity := flag.String("purity", "100", "Purity of the wallpapers")
	resolution := flag.String("reso", "1920x1080", "Resolution of Wallpaper")
	apiKey := flag.String("apikey", "", "API key for your account settings")
	baseURL := "http://wallhaven.cc/api/v1/search?"
	sortingType := flag.String("sort", "toplist", "Method of sorting result, (toplist, random, favorites,...)")
	outputDir := flag.String("output", os.Getenv("HOME")+"/Pictures/Wallpapers/", "Output Directory for the wallpaper file (Full path) Default: $HOME/Pictures/")
	flag.Parse()

	randomID := rand.New(rand.NewSource(time.Now().UnixNano())).Intn(500) // Getting top 500 wallpapers first
	pageNumber := randomID/24 + 1
	randomID %= 24

	var url string
	if *apiKey != "" {
		url = fmt.Sprintf("%sapikey=%s&categories=%s&purity=%s&resolution=%s&sorting=%s&page=%d", baseURL, *apiKey, *category, *purity, *resolution, *sortingType, pageNumber)
	} else {
		url = fmt.Sprintf("%scategories=%s&purity=%s&resolution=%s&sorting=%s&page=%d", baseURL, *category, *purity, *resolution, *sortingType, pageNumber)
	}

	response, err := http.Get(url)

	if err != nil {
		fmt.Print(err.Error())
		fmt.Fprintf(os.Stderr, "Well something is messed up.")
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}
	var responseObject Response
	json.Unmarshal(responseData, &responseObject)

	DownloadFile(responseObject.Data[randomID].Path, *outputDir+"wallhaven-"+responseObject.Data[randomID].ID+".jpg")
	time.Sleep(1) // to make sure file is written and saved

	// Setting the wallpaper
	out, err := exec.Command("gsettings", "set", "org.gnome.desktop.background", "picture-uri", "file://"+*outputDir+"wallhaven-"+responseObject.Data[randomID].ID+".jpg").Output()
	if err != nil {
		log.Fatal("Error setting wallpaper: ", err)
	}
	output := string(out[:])
	log.Output(10, output)
}
